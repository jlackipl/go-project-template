APP_NAME=project-template

## DOCKER config
DOCKER_RUNNER=docker run -u :$(shell id -g ${USER}) -v $(shell pwd):/app --network host ${APP_NAME}-builder 
RUNNER=

## GO config
GO=go
GO_TOOL=${GO} tool
GO_INSTALL=${GO} install
GO_VENDOR=GOSUMDB=off ${GO} mod vendor
GO_TIDY=GOSUMDB=off ${GO} mod tidy
GO_TEST=${GO} test -coverpkg=./... -coverprofile coverage.out ./...
GO_COVERAGE=${GO_TOOL} cover -func=coverage.out
GO_RUN=${GO} run

## OPENAPI config
OAPI_DOC=swagger/openapi.yaml
OAPI_SERVER_TYPE=chi-server
OAPI_OUTPUT_PATH=.
OAPI_PUBLIC_PATH=.

OAPI_SERVER_PKG_NAME=api
OAPI_SERVER_OUTPUT_PATH=${OAPI_OUTPUT_PATH}/${OAPI_SERVER_PKG_NAME}
OAPI_SERVER_FILE_NAME=server.gen.go

OAPI_CLIENT_PKG_NAME=api
OAPI_CLIENT_OUTPUT_PATH=${OAPI_PUBLIC_PATH}/${OAPI_CLIENT_PKG_NAME}
OAPI_CLIENT_FILE_NAME=http_client.gen.go
OAPI_CLIENT_TYPES_FILE_NAME=types.gen.go

OAPI_TYPES_PKG_NAME=api
OAPI_TYPES_OUTPUT_PATH=${OAPI_PUBLIC_PATH}/${OAPI_TYPES_PKG_NAME}
OAPI_TYPES_FILE_NAME=types.gen.go

##@ Local development
.PHONY: docker-image
docker-image: ## Build docker image
	@docker build -f infrastructure/docker/Dockerfile --target builder -t ${APP_NAME}-builder .

.PHONY: oapi-gen
oapi-gen:: oapi-server oapi-types oapi-client ## Generate server and types based on openapi file

.PHONY: oapi-server
oapi-server: ## Generate server based on openapi file
	@${DOCKER_RUNNER} mkdir -p ${OAPI_SERVER_OUTPUT_PATH}
	@${DOCKER_RUNNER} oapi-codegen -generate ${OAPI_SERVER_TYPE} -o ${OAPI_SERVER_OUTPUT_PATH}/${OAPI_SERVER_FILE_NAME} -package ${OAPI_SERVER_PKG_NAME} ${OAPI_DOC}

.PHONY: oapi-client
oapi-client: ## Generate server based on openapi file
	@${DOCKER_RUNNER} mkdir -p ${OAPI_SERVER_OUTPUT_PATH}
	@${DOCKER_RUNNER} oapi-codegen -generate client -o ${OAPI_CLIENT_OUTPUT_PATH}/${OAPI_CLIENT_FILE_NAME} -package ${OAPI_CLIENT_PKG_NAME} ${OAPI_DOC}

.PHONY: oapi-types
oapi-types: ## Generate types based on openapi file
	@${DOCKER_RUNNER} mkdir -p ${OAPI_TYPES_OUTPUT_PATH}
	@${DOCKER_RUNNER} oapi-codegen -generate types -o ${OAPI_TYPES_OUTPUT_PATH}/${OAPI_TYPES_FILE_NAME} -package ${OAPI_TYPES_PKG_NAME} ${OAPI_DOC}

.PHONY: dep
dep: ## Download dependencies
	@${RUNNER} ${GO_TIDY}

.PHONY: vendor
vendor: ## Download dependencies
	@${RUNNER} ${GO_VENDOR}

.PHONY: test
test: ## Run tests
	@${GO_TEST}
	@${GO_COVERAGE}

.PHONY: test-report
test-report: ## Run test with gotestsum
	@CGO_ENABLED=1 go install gotest.tools/gotestsum@latest
	@CGO_ENABLED=1 gotestsum --junitfile report.xml --format testname -- -mod vendor -timeout 300s -coverprofile=cover.out -race ./...

.PHONY: coverage
coverage: ## Create coverage
	@go test -v -coverpkg=./... -coverprofile .cover.out ./...
	@go tool cover -html=cover.out -o cover.html

##@ Help
.PHONY: help
help: ## Display Makefile help message
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make [STEP] [ENV_VARIALBE=value, ...; optional] \033[36m\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
